#include "quickwidget.h"
#include <qdebug.h>
static const char *textName = "txt";
static const char *windowName = "wnd";
QuickWidget::QuickWidget()
{
    connect(this, &QuickWidget::statusChanged, [this](Status status){
        qDebug() << __func__ << status;

        if (Ready == status)
        {
            findNeededItems();
        }
    });
}

void QuickWidget::mouseClickEvent()
{
        qDebug() << __func__;
    if(textItem)
    {
        static int i = 0;
        char text[255] = {};
        textItem->setProperty("text", itoa(++i, text, 10));

    }
}


void QuickWidget::findNeededItems()
{
    QQuickItem *root = this->rootObject();
    textItem = root->findChild<QQuickItem*>(textName);
    connect(root, SIGNAL(mouseClicked()), this, SLOT(mouseClickEvent()));
    qDebug() <<textItem;
}
