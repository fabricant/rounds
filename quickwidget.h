#ifndef QUICKWIDGET_H
#define QUICKWIDGET_H
#include <QQuickWidget>
#include <QQuickItem>


class QuickWidget : public QQuickWidget
{
    Q_OBJECT
public:
    QuickWidget();

public slots:
    void mouseClickEvent();
protected:
    void findNeededItems();
    QQuickItem *textItem;

};

#endif // QUICKWIDGET_H
