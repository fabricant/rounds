import QtQuick 2.12
import QtQuick.Window 2.12

Item {
    visible: true
    width: 640
    height: 480
    id:wnd
    objectName:"wnd"
    signal mouseClicked()
    onMouseClicked: {
        console.log("onMouseClicked")
    }

    Repeater{
        id:repeater
        model: ListModel {
            ListElement {
                //                id:roundRed
                color:"red"
            }
            ListElement {
                //                id:rounGreen
                color:"green"
            }
            ListElement {
                //                id:roundBlue
                color:"blue"
            }
            ListElement {
                //                id:roundOrange
                color:"orange"
            }
        }
        Item {
            width: 100
            height: 100
            id:rect
            Rectangle {
                id:round

                property int position: 0
                width: 100
                height: 100
                radius: width / 2
                color: modelData
                border.width: 3
                border.color: "black"
                AnchorAnimation {

                    easing.type: Easing.InOutQuad
                    duration: 1500

                }
                MouseArea {
                    anchors.fill: parent
                    onClicked:{
                        mouseClicked()
                        switch (parent.state){
                        case "TopLeft":
                            parent.state = "TopRight"
                            break

                        case "TopRight":
                            parent.state = "BottomRight"
                            break
                        case "BottomRight":
                            parent.state = "BottomLeft"
                            break
                        case "BottomLeft":
                            parent.state = "TopLeft"
                            break
                        default:
                            break;
                        }
                    }
                }

                states: [
                    State {
                        name: "TopLeft"
                        PropertyChanges{
                            target:round
                            x:0
                            y:0
                        }
                    },
                    State {
                        name: "TopRight"
                        PropertyChanges{
                            target:round
                            x:wnd.width - width
                            y:0
                        }
                    },
                    State {
                        name: "BottomRight"
                        PropertyChanges{
                            target:round
                            x:wnd.width - width
                            y:wnd.height - height
                        }
                    },
                    State {
                        name: "BottomLeft"
                        PropertyChanges{
                            target:round
                            x:0
                            y:wnd.height - height

                        }
                    }
                ]


                state: "TopLeft"

                transitions: [
                    Transition {
                        from: "TopLeft"
                        to: "TopRight"
                        PropertyAnimation {
                            target: round
                            properties: "x"
                            easing.type: Easing.InOutBounce
                        }
                    },
                    Transition {
                        from: "TopRight"
                        to: "BottomRight"
                        PropertyAnimation {
                            target: round
                            properties: "y"
                            easing.type: Easing.InOutBounce
                        }
                    },
                    Transition {
                        from: "BottomRight"
                        to: "BottomLeft"
                        PropertyAnimation {
                            target: round
                            properties: "x"
                            easing.type: Easing.InOutBounce
                        }
                    },
                    Transition {
                        from: "BottomLeft"
                        to: "TopLeft"
                        PropertyAnimation {
                            target: round
                            properties: "y"
                            easing.type: Easing.InOutBounce
                        }
                    }
                ]
            }

        }

    }

    Text {
        id: txt
        objectName: "txt"
        text: qsTr("0")
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
